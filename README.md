### 1. Terminal Stuff

- [Iterm 2](https://www.iterm2.com/)
- [XCode](http://osxdaily.com/2014/02/12/install-command-line-tools-mac-os-x/)
- [Node & NPM](http://blog.teamtreehouse.com/install-node-js-npm-mac)
- [Sass](http://sass-lang.com/install)
- [Gulp](https://semaphoreci.com/community/tutorials/getting-started-with-gulp-js)

----

### 2. Design Programs

- [Sketch App](https://www.sketchapp.com/)
- [Fontbase](http://fontba.se/)
- [Creative Cloud](http://www.adobe.com/ie/creativecloud.html)

----

### 3. Development Programs
- [Sublime Text 3](https://www.sublimetext.com/3)
- [Forklift](www.binarynights.com/forklift)
- [MAMP](https://www.mamp.info/en/)

----

### 4. Sublime Plugins

- [Package Control](https://packagecontrol.io/installation)
- [Emmet](https://emmet.io/)
- [SASS Syntax Highlighting](https://packagecontrol.io/packages/Sass)
- [Cobalt 2 Color Theme](https://github.com/wesbos/cobalt2)
- [Sidebar Enhancements](https://packagecontrol.io/packages/SideBarEnhancements)
- [Prettify](https://github.com/victorporof/Sublime-HTMLPrettify)
- [Advanced New File](https://packagecontrol.io/packages/AdvancedNewFile)
- [Bracket Highlighter](https://packagecontrol.io/packages/BracketHighlighter)

----

### 5. Project Management Stufdf/Communication

- [Slack](https://slack.com/downloads/osx)
- [Trello](https://trello.com/platforms)
- [Gitlab](https://gitlab.com/)

---

### 6. Alfred + Workflows

- [Alfred](https://www.alfredapp.com/)
- [Process Killer](https://github.com/ngreenstein/alfred-process-killer)
- [Placeholder Image Generator](https://gitlab.com/MarcMurray92/Placeholders-Workflow/)
- [Terminal > Finder](https://github.com/LeEnno/alfred-terminalfinder)

----

### 7. Utility

- [MacPass](https://github.com/mstarke/MacPass)